module otorank

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/mattn/go-sqlite3 v1.14.15
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
)
