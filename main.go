package main

import (
	"fmt"
	"otorank/database"
	"otorank/siteler"
)

func main() {
	fmt.Println("Otorank v0.0.4")

	db, err := database.NewSqlite()
	if err != nil {
		panic(err)
	}

	var vehicleList = siteler.ParseList()
	for i, a := range vehicleList {
		fmt.Println("### ilan -", i, "=================== ")
		fmt.Println("Model: ", a.Model)
		fmt.Println("Başlık: ", a.Title)
		fmt.Println("Yıl: ", a.Year)
		fmt.Println("KM: ", a.Kilometer)
		fmt.Println("Renk: ", a.Color)
		fmt.Println("Fiyat: ", a.Price)
		fmt.Println("Tarih: ", a.Date)
		fmt.Println("Sehir: ", a.CityTown)
		fmt.Println("======================== ")

		id, err := db.Insert(a)
		if err != nil {
			panic(err)
		}
		fmt.Println("Kayit olan id: ", id)
	}
}
