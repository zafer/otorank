package siteler

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"otorank/models"

	"github.com/PuerkitoBio/goquery"
)

const sahibindenLink = "https://www.sahibinden.com/renault-megane/dizel/otomatik"

func ParseList() []models.Vehicle {
	//var htmlData = s.RequestHTML(sahibindenLink)
	var htmlData = requestFile("html/sahibinden.html")

	doc, err := goquery.NewDocumentFromReader(htmlData)
	if err != nil {
		panic(err)
	}

	var adverts []models.Vehicle

	doc.Find(".searchResultsRowClass tr").Each(func(i int, s *goquery.Selection) {
		fmt.Println("i value: ", i)

		var a models.Vehicle
		//if i == 0 {
		model := s.Find(".searchResultsTagAttributeValue").Text()
		title := s.Find(".searchResultsTitleValue a").Text()
		price := s.Find(".searchResultsPriceValue").Text()

		year, km, color := getAttributes(s)

		var date string
		s.Find(".searchResultsDateValue span").Each(func(i int, s *goquery.Selection) {
			datePart := s.Text()
			if datePart != " " {
				date += datePart + " "
			}
		})

		cityTownValue, _ := s.Find(".searchResultsLocationValue").Html()
		cityTown := strings.Replace(cityTownValue, "<br/>", "/", -1)

		a.Model = strings.TrimSpace(model)
		a.Title = strings.TrimSpace(title)
		a.Year = strings.TrimSpace(year)
		a.Kilometer = strings.TrimSpace(km)
		a.Color = strings.TrimSpace(color)
		a.Price = strings.TrimSpace(price)
		a.Date = strings.TrimSpace(date)
		a.CityTown = strings.TrimSpace(cityTown)

		adverts = append(adverts, a)
		//}

		//i++
	})

	return adverts
}

func getAttributes(s *goquery.Selection) (year string, km string, color string) {
	var attrList []string
	s.Find(".searchResultsAttributeValue").Each(func(k int, s *goquery.Selection) {
		attr, _ := s.Html()
		attrList = append(attrList, strings.TrimSpace(attr))
	})

	if len(attrList) > 0 {
		year = attrList[0]
	}

	if len(attrList) > 1 {
		km = attrList[1]
	}

	if len(attrList) > 2 {
		color = attrList[2]
	}

	return year, km, color
}

func requestHTML(URL string) io.Reader {
	// Request the HTML page.
	res, err := http.Get(URL)
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	return res.Body
}

func requestFile(address string) io.Reader {
	file, _ := os.Open(address)

	return file
}
