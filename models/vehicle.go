package models

type Vehicle struct {
	Model     string
	Title     string
	Year      string
	Kilometer string
	Color     string
	Price     string
	Date      string
	CityTown  string
}
