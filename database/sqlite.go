package database

import (
	"database/sql"
	"otorank/models"

	_ "github.com/mattn/go-sqlite3"
)

const filepath string = "otorank.db"

const create string = `
		CREATE TABLE IF NOT EXISTS vehicle (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			model TEXT NOT NULL,
			title TEXT NOT NULL,
			year INTEGER NOT NULL,
			odometer TEXT NOT NULL,
			color TEXT NOT NULL,
			price TEXT NOT NULL,
			date TEXT NOT NULL,
			city TEXT NOT NULL
		);`

type Sqlite struct {
	db *sql.DB
}

func NewSqlite() (*Sqlite, error) {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(create)
	if err != nil {
		return nil, err
	}

	return &Sqlite{db: db}, nil
}

func (s *Sqlite) Insert(v models.Vehicle) (int, error) {
	var sql = `INSERT INTO vehicle(model,title,year,odometer,color,price,date,city) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)`
	res, err := s.db.Exec(sql, v.Model, v.Title, v.Year, v.Kilometer, v.Color, v.Price, v.Date, v.CityTown)
	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}
